/* tslint:disable */
/* eslint-disable */
/**
 * Первая автоколонна API
 * `ВНИМАНИЕ! Это бета варсия. О возникновении ошибок 500, неверной работе методов и ошибок в документации сообщайте письмом на RGruk@fcirkutsk.ru с темой API. Мы постараемся как можно быстрее отреагировать.`  Все запросы к API должны выполняться по протоколу __HTTPS__.  Авторизация базовая, логин выдаётся при регистрации.  Мы поддерживаем HTTP-методы __GET__ и __POST__. Мы поддерживаем три способа передачи параметров в запросах:  - __GET__:   - Строка запроса URL - __POST__:   - application/x-www-form-urlencoded   - application/json  При использовании __POST__ запроса с параметрами необходимо передать заголовок __\'Content-Type\'__ со значением __\'application/x-www-form-urlencoded\'__ или __\'application/json\'__. Другие типы содержимого не поддерживаются.  Для __GET__ запросов без параметров __POST__ методы не описаны, но они тоже будут работать. Заголовок __\'Content-Type\'__, если он указан, в этом случае будет проигнорирован.  Если описан только метод __POST__, значит данный запрос можно выполнить только этим методом.  Ответ содержит заголовок __\'Content-Type\'__ со значением __\'application/json\'__ и объект JSON, который всегда имеет логическое поле \'ok\' и строковое поле \'description\' с удобочитаемым описанием результата. Если \'ok\' равно true, запрос был выполнен успешно, и результат запроса можно найти в поле \'result\'. В случае неудачного запроса \'ok\' равно false, и ошибка объясняется в \'description\' (в некоторых случаях дополнительно поле \'result\' содержит объект, имена полей которого указывают на места возникновения ошибок, а содержимое содержит описание ошибки). Также возвращается целочисленное поле \'code\', в нем дублируется код состояния HTTP.  Для ответов, предполагающих в поле \'result\' массив, возможен возврат пустого массива в случае отсутствия данных по запрошеным параметрам.  Так же возможен ответ с типом сожержимого __\'Content-Type\'__ со значением __\'text/plain\'__ и описанием причины ошибки в теле запроса в случае возникновения ошибок низкого уровня (500, 401, 403, 404, 405).  Все методы __чувствительны к регистру__. Все запросы используют кодировку __UTF-8__.  ___Для отладки используйте тестовый сервис [https://fcirkutsk.ru/api/test](https://fcirkutsk.ru/api/test)___
 *
 * The version of the OpenAPI document: 0.9.b
 * Contact: admin@fcirkutsk.ru
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import type { Configuration } from './configuration';
import type { AxiosPromise, AxiosInstance, AxiosRequestConfig } from 'axios';
import globalAxios from 'axios';
// Some imports not used depending on template conditions
// @ts-ignore
import { DUMMY_BASE_URL, assertParamExists, setApiKeyToObject, setBasicAuthToObject, setBearerAuthToObject, setOAuthToObject, setSearchParams, serializeDataIfNeeded, toPathString, createRequestFunction } from './common';
import type { RequestArgs } from './base';
// @ts-ignore
import { BASE_PATH, COLLECTION_FORMATS, BaseAPI, RequiredError } from './base';

/**
 * 
 * @export
 * @interface Artisan
 */
export interface Artisan {
    /**
     * 
     * @type {string}
     * @memberof Artisan
     */
    'id': string;
    /**
     * 
     * @type {string}
     * @memberof Artisan
     */
    'name': string;
}
/**
 * 
 * @export
 * @interface ArtisanTimes
 */
export interface ArtisanTimes {
    /**
     * 
     * @type {Array<string>}
     * @memberof ArtisanTimes
     */
    'result'?: Array<string>;
}
/**
 * 
 * @export
 * @interface Artisans
 */
export interface Artisans {
    /**
     * 
     * @type {Array<Artisan>}
     * @memberof Artisans
     */
    'result'?: Array<Artisan>;
}
/**
 * Общая схема ответа сервиса
 * @export
 * @interface BaseResponse
 */
export interface BaseResponse {
    /**
     * Признак успешности операции. Содержит true если успешно.
     * @type {boolean}
     * @memberof BaseResponse
     */
    'ok': boolean;
    /**
     * Код возврата, дублирует код состояния HTTP. Коды меньше 400 не являются ошибками.
     * @type {number}
     * @memberof BaseResponse
     */
    'code': number;
    /**
     * Текстовое описание причины ошибки. При отсутствии ошибки содержит \"ОК\"
     * @type {string}
     * @memberof BaseResponse
     */
    'description': string;
    /**
     * 
     * @type {BaseResponseResult}
     * @memberof BaseResponse
     */
    'result'?: BaseResponseResult;
}
/**
 * @type BaseResponseResult
 * Результат работы метода, тип зависит от метода и его параметров.
 * @export
 */
export type BaseResponseResult = Array<BaseResponseResultOneOfInner> | string;

/**
 * @type BaseResponseResultOneOfInner
 * @export
 */
export type BaseResponseResultOneOfInner = number | object | string;

/**
 * 
 * @export
 * @interface Brand
 */
export interface Brand {
    /**
     * 
     * @type {string}
     * @memberof Brand
     */
    'id': string;
    /**
     * 
     * @type {string}
     * @memberof Brand
     */
    'name': string;
}
/**
 * 
 * @export
 * @interface Brands
 */
export interface Brands {
    /**
     * 
     * @type {Array<Brand>}
     * @memberof Brands
     */
    'result'?: Array<Brand>;
}
/**
 * 
 * @export
 * @interface CreateTicketPost200Response
 */
export interface CreateTicketPost200Response {
    /**
     * 
     * @type {string}
     * @memberof CreateTicketPost200Response
     */
    'result'?: string;
}
/**
 * @type GetArtisanTimeGetDateParameter
 * @export
 */
export type GetArtisanTimeGetDateParameter = number | string;

/**
 * 
 * @export
 * @interface GetArtisanTimePostRequest
 */
export interface GetArtisanTimePostRequest {
    /**
     * 
     * @type {string}
     * @memberof GetArtisanTimePostRequest
     */
    'artisan_id': string;
    /**
     * Дата для записи. Универсальное время в формате ISO или Unix. Если не указан - используется текущая дата.
     * @type {string}
     * @memberof GetArtisanTimePostRequest
     */
    'date'?: string;
    /**
     * Формат возвращаемых дат
     * @type {string}
     * @memberof GetArtisanTimePostRequest
     */
    'format'?: GetArtisanTimePostRequestFormatEnum;
}

export const GetArtisanTimePostRequestFormatEnum = {
    Iso: 'iso',
    Unix: 'unix',
    Javascript: 'javascript',
    Microsoft: 'microsoft'
} as const;

export type GetArtisanTimePostRequestFormatEnum = typeof GetArtisanTimePostRequestFormatEnum[keyof typeof GetArtisanTimePostRequestFormatEnum];

/**
 * 
 * @export
 * @interface GetModelsPostRequest
 */
export interface GetModelsPostRequest {
    /**
     * Уникальный идентификатор производителя (брэнда).
     * @type {string}
     * @memberof GetModelsPostRequest
     */
    'brand_id': string;
}
/**
 * 
 * @export
 * @interface Model
 */
export interface Model {
    /**
     * 
     * @type {string}
     * @memberof Model
     */
    'id': string;
    /**
     * 
     * @type {string}
     * @memberof Model
     */
    'name': string;
    /**
     * 
     * @type {string}
     * @memberof Model
     */
    'full_name': string;
}
/**
 * 
 * @export
 * @interface Models
 */
export interface Models {
    /**
     * 
     * @type {Array<Model>}
     * @memberof Models
     */
    'result'?: Array<Model>;
}
/**
 * 
 * @export
 * @interface Service
 */
export interface Service {
    /**
     * 
     * @type {string}
     * @memberof Service
     */
    'id': string;
    /**
     * 
     * @type {string}
     * @memberof Service
     */
    'name': string;
}
/**
 * 
 * @export
 * @interface Services
 */
export interface Services {
    /**
     * 
     * @type {Array<Service>}
     * @memberof Services
     */
    'result'?: Array<Service>;
}
/**
 * 
 * @export
 * @interface Ticket
 */
export interface Ticket {
    /**
     * 
     * @type {string}
     * @memberof Ticket
     */
    'brand_id': string;
    /**
     * 
     * @type {string}
     * @memberof Ticket
     */
    'model_id': string;
    /**
     * Уникальный VIN номер автомобиля
     * @type {string}
     * @memberof Ticket
     */
    'vin': string;
    /**
     * Государственный номер
     * @type {string}
     * @memberof Ticket
     */
    'number': string;
    /**
     * Год выпуска автомобиля
     * @type {number}
     * @memberof Ticket
     */
    'year': number;
    /**
     * Обращение к клиенту
     * @type {string}
     * @memberof Ticket
     */
    'name': string;
    /**
     * Контактный телефонный номер клиента
     * @type {string}
     * @memberof Ticket
     */
    'phone': string;
    /**
     * 
     * @type {string}
     * @memberof Ticket
     */
    'service_id': string;
    /**
     * 
     * @type {string}
     * @memberof Ticket
     */
    'artisan_id'?: string;
    /**
     * Желаемое время записи
     * @type {string}
     * @memberof Ticket
     */
    'time'?: string;
    /**
     * Прочие детали заявки
     * @type {string}
     * @memberof Ticket
     */
    'comment'?: string;
}

/**
 * DefaultApi - axios parameter creator
 * @export
 */
export const DefaultApiAxiosParamCreator = function (configuration?: Configuration) {
    return {
        /**
         * В случае успешного создания заявки возвращает её идентификатор.
         * @summary Создать заявку на сервисные работы.
         * @param {Ticket} [ticket] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        createTicketPost: async (ticket?: Ticket, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/create_ticket`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'POST', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;


    
            localVarHeaderParameter['Content-Type'] = 'application/json';

            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};
            localVarRequestOptions.data = serializeDataIfNeeded(ticket, localVarRequestOptions, configuration)

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * Если параметр __format__ содежит \'unix\' - возвращает массив целых чисел, если параметр __format__ содержит \'iso\' или не указан - возвращает массив строк. Если параметр __date__ не указан - используется текущая дата.
         * @summary Получить возможное время записи к мастеру.
         * @param {string} artisanId Уникальный идентификатор мастера.
         * @param {GetArtisanTimeGetDateParameter} [date] Дата для записи. Универсальное время в формате ISO или Unix.
         * @param {GetArtisanTimeGetFormatEnum} [format] Формат возвращаемых дат
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getArtisanTimeGet: async (artisanId: string, date?: GetArtisanTimeGetDateParameter, format?: GetArtisanTimeGetFormatEnum, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            // verify required parameter 'artisanId' is not null or undefined
            assertParamExists('getArtisanTimeGet', 'artisanId', artisanId)
            const localVarPath = `/get_artisan_time`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            if (artisanId !== undefined) {
                localVarQueryParameter['artisan_id'] = artisanId;
            }

            if (date !== undefined) {
                localVarQueryParameter['date'] = date;
            }

            if (format !== undefined) {
                localVarQueryParameter['format'] = format;
            }


    
            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * Если параметр __format__ содежит \'unix\' - возвращает массив целых чисел, если параметр __format__ содержит \'iso\' или не указан - возвращает массив строк. Если параметр __date__ не указан - используется текущая дата.
         * @summary Получить возможное время записи к мастеру.
         * @param {GetArtisanTimePostRequest} [getArtisanTimePostRequest] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getArtisanTimePost: async (getArtisanTimePostRequest?: GetArtisanTimePostRequest, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/get_artisan_time`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'POST', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;


    
            localVarHeaderParameter['Content-Type'] = 'application/json';

            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};
            localVarRequestOptions.data = serializeDataIfNeeded(getArtisanTimePostRequest, localVarRequestOptions, configuration)

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * Список может отличаться в зависимости от профиля вашей учётной записи (согласуется при регистрации).
         * @summary Получить список мастеров.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getArtisansGet: async (options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/get_artisans`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;


    
            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * Список может отличаться в зависимости от профиля вашей учётной записи (согласуется при регистрации).
         * @summary Получить список производителей (брэндов)
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getBrandsGet: async (options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/get_brands`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;


    
            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @summary Получить список моделей по идентификатору производителея (брэнда).
         * @param {string} brandId Уникальный идентификатор производителя (брэнда).
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getModelsGet: async (brandId: string, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            // verify required parameter 'brandId' is not null or undefined
            assertParamExists('getModelsGet', 'brandId', brandId)
            const localVarPath = `/get_models`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            if (brandId !== undefined) {
                localVarQueryParameter['brand_id'] = brandId;
            }


    
            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @summary Получить список моделей по идентификатору производителея (брэнда).
         * @param {GetModelsPostRequest} [getModelsPostRequest] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getModelsPost: async (getModelsPostRequest?: GetModelsPostRequest, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/get_models`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'POST', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;


    
            localVarHeaderParameter['Content-Type'] = 'application/json';

            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};
            localVarRequestOptions.data = serializeDataIfNeeded(getModelsPostRequest, localVarRequestOptions, configuration)

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * Список может отличаться в зависимости от профиля вашей учётной записи (согласуется при регистрации).
         * @summary Получить список производимых работ
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getServicesGet: async (options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/get_services`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;


    
            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
    }
};

/**
 * DefaultApi - functional programming interface
 * @export
 */
export const DefaultApiFp = function(configuration?: Configuration) {
    const localVarAxiosParamCreator = DefaultApiAxiosParamCreator(configuration)
    return {
        /**
         * В случае успешного создания заявки возвращает её идентификатор.
         * @summary Создать заявку на сервисные работы.
         * @param {Ticket} [ticket] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async createTicketPost(ticket?: Ticket, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<CreateTicketPost200Response>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.createTicketPost(ticket, options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
        /**
         * Если параметр __format__ содежит \'unix\' - возвращает массив целых чисел, если параметр __format__ содержит \'iso\' или не указан - возвращает массив строк. Если параметр __date__ не указан - используется текущая дата.
         * @summary Получить возможное время записи к мастеру.
         * @param {string} artisanId Уникальный идентификатор мастера.
         * @param {GetArtisanTimeGetDateParameter} [date] Дата для записи. Универсальное время в формате ISO или Unix.
         * @param {GetArtisanTimeGetFormatEnum} [format] Формат возвращаемых дат
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async getArtisanTimeGet(artisanId: string, date?: GetArtisanTimeGetDateParameter, format?: GetArtisanTimeGetFormatEnum, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<ArtisanTimes>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.getArtisanTimeGet(artisanId, date, format, options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
        /**
         * Если параметр __format__ содежит \'unix\' - возвращает массив целых чисел, если параметр __format__ содержит \'iso\' или не указан - возвращает массив строк. Если параметр __date__ не указан - используется текущая дата.
         * @summary Получить возможное время записи к мастеру.
         * @param {GetArtisanTimePostRequest} [getArtisanTimePostRequest] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async getArtisanTimePost(getArtisanTimePostRequest?: GetArtisanTimePostRequest, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<ArtisanTimes>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.getArtisanTimePost(getArtisanTimePostRequest, options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
        /**
         * Список может отличаться в зависимости от профиля вашей учётной записи (согласуется при регистрации).
         * @summary Получить список мастеров.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async getArtisansGet(options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<Artisans>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.getArtisansGet(options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
        /**
         * Список может отличаться в зависимости от профиля вашей учётной записи (согласуется при регистрации).
         * @summary Получить список производителей (брэндов)
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async getBrandsGet(options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<Brands>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.getBrandsGet(options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
        /**
         * 
         * @summary Получить список моделей по идентификатору производителея (брэнда).
         * @param {string} brandId Уникальный идентификатор производителя (брэнда).
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async getModelsGet(brandId: string, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<Models>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.getModelsGet(brandId, options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
        /**
         * 
         * @summary Получить список моделей по идентификатору производителея (брэнда).
         * @param {GetModelsPostRequest} [getModelsPostRequest] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async getModelsPost(getModelsPostRequest?: GetModelsPostRequest, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<Models>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.getModelsPost(getModelsPostRequest, options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
        /**
         * Список может отличаться в зависимости от профиля вашей учётной записи (согласуется при регистрации).
         * @summary Получить список производимых работ
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async getServicesGet(options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<Services>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.getServicesGet(options);
            return createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration);
        },
    }
};

/**
 * DefaultApi - factory interface
 * @export
 */
export const DefaultApiFactory = function (configuration?: Configuration, basePath?: string, axios?: AxiosInstance) {
    const localVarFp = DefaultApiFp(configuration)
    return {
        /**
         * В случае успешного создания заявки возвращает её идентификатор.
         * @summary Создать заявку на сервисные работы.
         * @param {Ticket} [ticket] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        createTicketPost(ticket?: Ticket, options?: any): AxiosPromise<CreateTicketPost200Response> {
            return localVarFp.createTicketPost(ticket, options).then((request) => request(axios, basePath));
        },
        /**
         * Если параметр __format__ содежит \'unix\' - возвращает массив целых чисел, если параметр __format__ содержит \'iso\' или не указан - возвращает массив строк. Если параметр __date__ не указан - используется текущая дата.
         * @summary Получить возможное время записи к мастеру.
         * @param {string} artisanId Уникальный идентификатор мастера.
         * @param {GetArtisanTimeGetDateParameter} [date] Дата для записи. Универсальное время в формате ISO или Unix.
         * @param {GetArtisanTimeGetFormatEnum} [format] Формат возвращаемых дат
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getArtisanTimeGet(artisanId: string, date?: GetArtisanTimeGetDateParameter, format?: GetArtisanTimeGetFormatEnum, options?: any): AxiosPromise<ArtisanTimes> {
            return localVarFp.getArtisanTimeGet(artisanId, date, format, options).then((request) => request(axios, basePath));
        },
        /**
         * Если параметр __format__ содежит \'unix\' - возвращает массив целых чисел, если параметр __format__ содержит \'iso\' или не указан - возвращает массив строк. Если параметр __date__ не указан - используется текущая дата.
         * @summary Получить возможное время записи к мастеру.
         * @param {GetArtisanTimePostRequest} [getArtisanTimePostRequest] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getArtisanTimePost(getArtisanTimePostRequest?: GetArtisanTimePostRequest, options?: any): AxiosPromise<ArtisanTimes> {
            return localVarFp.getArtisanTimePost(getArtisanTimePostRequest, options).then((request) => request(axios, basePath));
        },
        /**
         * Список может отличаться в зависимости от профиля вашей учётной записи (согласуется при регистрации).
         * @summary Получить список мастеров.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getArtisansGet(options?: any): AxiosPromise<Artisans> {
            return localVarFp.getArtisansGet(options).then((request) => request(axios, basePath));
        },
        /**
         * Список может отличаться в зависимости от профиля вашей учётной записи (согласуется при регистрации).
         * @summary Получить список производителей (брэндов)
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getBrandsGet(options?: any): AxiosPromise<Brands> {
            return localVarFp.getBrandsGet(options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @summary Получить список моделей по идентификатору производителея (брэнда).
         * @param {string} brandId Уникальный идентификатор производителя (брэнда).
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getModelsGet(brandId: string, options?: any): AxiosPromise<Models> {
            return localVarFp.getModelsGet(brandId, options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @summary Получить список моделей по идентификатору производителея (брэнда).
         * @param {GetModelsPostRequest} [getModelsPostRequest] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getModelsPost(getModelsPostRequest?: GetModelsPostRequest, options?: any): AxiosPromise<Models> {
            return localVarFp.getModelsPost(getModelsPostRequest, options).then((request) => request(axios, basePath));
        },
        /**
         * Список может отличаться в зависимости от профиля вашей учётной записи (согласуется при регистрации).
         * @summary Получить список производимых работ
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getServicesGet(options?: any): AxiosPromise<Services> {
            return localVarFp.getServicesGet(options).then((request) => request(axios, basePath));
        },
    };
};

/**
 * DefaultApi - object-oriented interface
 * @export
 * @class DefaultApi
 * @extends {BaseAPI}
 */
export class DefaultApi extends BaseAPI {
    /**
     * В случае успешного создания заявки возвращает её идентификатор.
     * @summary Создать заявку на сервисные работы.
     * @param {Ticket} [ticket] 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof DefaultApi
     */
    public createTicketPost(ticket?: Ticket, options?: AxiosRequestConfig) {
        return DefaultApiFp(this.configuration).createTicketPost(ticket, options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * Если параметр __format__ содежит \'unix\' - возвращает массив целых чисел, если параметр __format__ содержит \'iso\' или не указан - возвращает массив строк. Если параметр __date__ не указан - используется текущая дата.
     * @summary Получить возможное время записи к мастеру.
     * @param {string} artisanId Уникальный идентификатор мастера.
     * @param {GetArtisanTimeGetDateParameter} [date] Дата для записи. Универсальное время в формате ISO или Unix.
     * @param {GetArtisanTimeGetFormatEnum} [format] Формат возвращаемых дат
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof DefaultApi
     */
    public getArtisanTimeGet(artisanId: string, date?: GetArtisanTimeGetDateParameter, format?: GetArtisanTimeGetFormatEnum, options?: AxiosRequestConfig) {
        return DefaultApiFp(this.configuration).getArtisanTimeGet(artisanId, date, format, options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * Если параметр __format__ содежит \'unix\' - возвращает массив целых чисел, если параметр __format__ содержит \'iso\' или не указан - возвращает массив строк. Если параметр __date__ не указан - используется текущая дата.
     * @summary Получить возможное время записи к мастеру.
     * @param {GetArtisanTimePostRequest} [getArtisanTimePostRequest] 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof DefaultApi
     */
    public getArtisanTimePost(getArtisanTimePostRequest?: GetArtisanTimePostRequest, options?: AxiosRequestConfig) {
        return DefaultApiFp(this.configuration).getArtisanTimePost(getArtisanTimePostRequest, options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * Список может отличаться в зависимости от профиля вашей учётной записи (согласуется при регистрации).
     * @summary Получить список мастеров.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof DefaultApi
     */
    public getArtisansGet(options?: AxiosRequestConfig) {
        return DefaultApiFp(this.configuration).getArtisansGet(options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * Список может отличаться в зависимости от профиля вашей учётной записи (согласуется при регистрации).
     * @summary Получить список производителей (брэндов)
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof DefaultApi
     */
    public getBrandsGet(options?: AxiosRequestConfig) {
        return DefaultApiFp(this.configuration).getBrandsGet(options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * 
     * @summary Получить список моделей по идентификатору производителея (брэнда).
     * @param {string} brandId Уникальный идентификатор производителя (брэнда).
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof DefaultApi
     */
    public getModelsGet(brandId: string, options?: AxiosRequestConfig) {
        return DefaultApiFp(this.configuration).getModelsGet(brandId, options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * 
     * @summary Получить список моделей по идентификатору производителея (брэнда).
     * @param {GetModelsPostRequest} [getModelsPostRequest] 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof DefaultApi
     */
    public getModelsPost(getModelsPostRequest?: GetModelsPostRequest, options?: AxiosRequestConfig) {
        return DefaultApiFp(this.configuration).getModelsPost(getModelsPostRequest, options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * Список может отличаться в зависимости от профиля вашей учётной записи (согласуется при регистрации).
     * @summary Получить список производимых работ
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof DefaultApi
     */
    public getServicesGet(options?: AxiosRequestConfig) {
        return DefaultApiFp(this.configuration).getServicesGet(options).then((request) => request(this.axios, this.basePath));
    }
}

/**
 * @export
 */
export const GetArtisanTimeGetFormatEnum = {
    Iso: 'iso',
    Unix: 'unix',
    Javascript: 'javascript',
    Microsoft: 'microsoft'
} as const;
export type GetArtisanTimeGetFormatEnum = typeof GetArtisanTimeGetFormatEnum[keyof typeof GetArtisanTimeGetFormatEnum];


