import { useCallback, useEffect, useReducer } from 'react';
import type { BaseResponseResult } from '@/api';
import { AxiosResponse } from 'axios';

export type CacheData = {
  [key: string]: BaseResponseResult;
};

export type ReducerState = {
  status: 'idle' | 'fetching' | 'fetched' | 'error' | 'validation_error';
  error?: BaseResponseResult | null;
  data?: BaseResponseResult | string;
};

export type ReducerAction = {
  type: 'FETCHING' | 'FETCHED' | 'FETCHED_VALIDATION_ERROR' | 'FETCH_ERROR';
  payload?: BaseResponseResult | string;
};

const cacheData: CacheData = {};

const initialState: ReducerState = {
  status: 'idle',
  error: null,
  data: [],
};
export const useApi = (id: string, request: () => Promise<AxiosResponse>, requestCondition: boolean = true) => {
  const [state, dispatch] = useReducer((state: ReducerState, action: ReducerAction): ReducerState => {
    switch (action.type) {
      case 'FETCHING':
        return { ...initialState, status: 'fetching' };
      case 'FETCHED':
        return { ...initialState, status: 'fetched', data: action.payload };
      case 'FETCHED_VALIDATION_ERROR':
        return { ...initialState, status: 'validation_error', data: action.payload };
      case 'FETCH_ERROR':
        return { ...initialState, status: 'error', error: action.payload, data: action.payload };
      default:
        return state;
    }
  }, initialState);
  const renderData = useCallback(async () => {
    dispatch({ type: 'FETCHING' });
    if (cacheData[id]) {
      const data = cacheData[id];
      dispatch({ type: 'FETCHED', payload: data });
    } else {
      try {
        const data = await request();
        cacheData[id] = data.data.result;
        dispatch({ type: 'FETCHED', payload: data.data.result });
      } catch (error: any) {
        if (error?.response?.data?.description === 'Ошибка в параметрах') {
          dispatch({ type: 'FETCHED_VALIDATION_ERROR', payload: error.response.data?.result });
        } else {
          dispatch({ type: 'FETCH_ERROR', payload: error?.response?.data?.message });
        }
        delete cacheData[id];
      }
    }
  }, [id, request]);

  useEffect(() => {
    if (requestCondition) {
      renderData();
    }
  }, [requestCondition]);
  return state;
};
