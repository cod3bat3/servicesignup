import { useMemo } from 'react';
import { Ticket } from '@/api';

export const useRequiredFields = (form: Partial<Ticket>, reqKey: keyof Ticket): Partial<Ticket> => {
  return useMemo(() => {
    if (reqKey && form[reqKey]) {
      return { [reqKey]: form[reqKey] };
    }
    return {};
  }, [form[reqKey]]);
};
