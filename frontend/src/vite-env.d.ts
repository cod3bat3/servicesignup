/// <reference types="vite/client" />
import path from 'path';
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

export default defineConfig({
  plugins: [react()],
  resolve: {
    '@': path.resolve(__dirname, './src'),
  },
});
