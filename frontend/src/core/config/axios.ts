import axios from "axios";

axios.defaults.baseURL = import.meta.env.VITE_BASE_API
axios.defaults.auth = {username: import.meta.env.VITE_AUTH_USER, password: import.meta.env.VITE_AUTH_PASSWORD}

export default axios