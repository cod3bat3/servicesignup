import styles from './index.module.scss';
import { FC, useEffect, useState } from 'react';
import { FORM_CONFIG, FormStep, FormField, FormNavigation, FormHint } from './shared';
import type { FieldResponse, DefaultObject } from './shared';
import { useApi } from '@/hooks/useApi';
import { DefaultApi, Ticket } from '@/api';

const maxStep: number = Object.entries(FORM_CONFIG).length - 1;
const Api = new DefaultApi();
export const Form: FC = () => {
    const [ form, setForm ] = useState<Partial<Ticket>>({});
    const [ step, setStep ] = useState<number>(0);
    const [ errorsList, setErrorsList ] = useState<DefaultObject[]>([]);
    const [ isFormReady, setIsFormReady ] = useState<boolean>(false);
    const { data, status } = useApi('ticket', () => Api.createTicketPost(form as Ticket), isFormReady);

    useEffect(() => {
        setIsFormReady(false);
        if (status === 'validation_error') {
            setErrorsList(data as DefaultObject[]);
        }
    }, [ data ]);

    const onNextClickHandler = () => (step < maxStep ? setStep(( step ) => ++step) : setIsFormReady(true));

    const onPrevClickHandler = () => setStep(( step ) => --step);

    const buttonText = step < maxStep ? 'Следующий шаг' : 'Отправить заявку';
    const onChangeHandler = ( e: FieldResponse ) =>
        setForm(( prevState ) => ({
            ...prevState,
            [e.key]: e.value,
        }));

    return (
        <div className={ styles.form }>
            <h2 className={ styles.form__title }>Онлайн запись на сервис</h2>
            <FormHint isError={ !!errorsList.length }
                      isSent={ status === 'fetched' }
            />
            { status !== 'fetched' && (
                <form className={ styles.form__form }>
                    { FORM_CONFIG[step].map(( { title, items, key } ) => (
                        <FormStep key={ key }
                                  title={ title }
                        >
                            { items.map(( item ) => (
                                <FormField
                                    key={ item.key }
                                    item={ item }
                                    value={ form[item.key] }
                                    errorsList={ errorsList }
                                    form={ form as Ticket }
                                    onChange={ onChangeHandler }
                                />
                            )) }
                        </FormStep>
                    )) }
                    <FormNavigation
                        step={ step }
                        buttonText={ buttonText }
                        disabled={ status === 'fetching' }
                        onNextClick={ onNextClickHandler }
                        onPrevClick={ onPrevClickHandler }
                    />
                </form>
            ) }
            <img src='/images/carBg.png'
                 alt=''
                 className={ styles.form__image }
            />
            <img src='/images/car.png'
                 alt=''
                 className={ styles.form__image }
            />
        </div>
    );
};
