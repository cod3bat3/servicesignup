import type { MaskitoMask } from '@maskito/core';
import type { Ticket } from '@/api';

export interface FieldItem {
  key: keyof Ticket;
  label: string;
  type: string;
  itemsApi?: string;
  required: keyof Ticket | '';
}

export interface FieldInputItem extends FieldItem {
  mask?: MaskitoMask;
}

export interface FieldDateItem extends FieldItem {
  showTime?: boolean;
}

export interface FieldSelectItem extends FieldItem {}

export type Step = {
  key: string;
  title: string;
  items: FieldInputItem[] | FieldSelectItem[] | FieldDateItem[];
};

export type FieldResponse = {
  key: string;
  value: string;
};

export type DefaultObject = {
  [key: string]: string;
};

export type FormObject = DefaultObject;

export type SelectValue = DefaultObject | string;
