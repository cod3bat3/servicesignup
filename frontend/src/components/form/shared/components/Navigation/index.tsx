import styles from './index.module.scss';
import { FC, MouseEvent } from 'react';
import { BaseButton } from '@/components/ui';
import { ArrowRight } from '@/components/icons';

export type FormNavigationProps = {
  buttonText: string;
  step: number;
  disabled: boolean;
  onNextClick: (e: MouseEvent<HTMLButtonElement>) => void;
  onPrevClick: (e: MouseEvent<HTMLButtonElement>) => void;
};
export const FormNavigation: FC<FormNavigationProps> = ({ buttonText, step, disabled, onNextClick, onPrevClick }) => {
  return (
    <div className={styles.navigation}>
      <div className={styles.navigation__buttons}>
        {step > 0 && (
          <BaseButton
            onClick={onPrevClick}
            mode={'outlined'}
            className={styles.navigation__buttonBack}
            disabled={disabled}
          >
            <ArrowRight />
          </BaseButton>
        )}
        <BaseButton onClick={onNextClick} className={styles.navigation__button} disabled={disabled}>
          {buttonText}
        </BaseButton>
      </div>
      <p className={styles.navigation__disclaimer}>
        Нажимая кнопку "{buttonText}", Вы <br></br> соглашаетесь с Политикой защиты данных.
      </p>
    </div>
  );
};
