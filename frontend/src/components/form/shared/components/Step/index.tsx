import styles from './index.module.scss';
import { FC, PropsWithChildren } from 'react';

type FormStepProps = {
  title?: string;
};

export const FormStep: FC<PropsWithChildren<FormStepProps>> = ({ children, title }) => {
  return (
    <div className={styles['form-Step']}>
      {title && <h3 className={styles['form-step__subtitle']}>{title}</h3>}
      <div className={styles['form-step__constructor']}>{children}</div>
    </div>
  );
};
