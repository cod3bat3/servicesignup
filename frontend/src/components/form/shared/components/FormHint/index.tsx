import styles from './index.module.scss';
import { FC, useMemo } from 'react';
import { clsx } from 'clsx';

export type FormHintProps = {
  isError: boolean;
  isSent: boolean;
};

export const FormHint: FC<FormHintProps> = ({ isError, isSent }) => {
  const text = useMemo(() => {
    if (isSent) return 'Вы успешно записались на сервис!';
    else if (isError) return 'Все поля должны быть заполнены!';
    return '';
  }, [isError, isSent]);
  return (
    <div className={clsx(styles.hint, { [styles['hint--success']]: isSent, [styles['hint--error']]: isError })}>
      {text}
    </div>
  );
};
