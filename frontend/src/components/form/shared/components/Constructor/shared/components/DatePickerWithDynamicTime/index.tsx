import { ChangeEvent, FC, useEffect, useState } from 'react';
import { FormObject } from '@/components/form/shared';
import { BaseDatePicker } from '@/components/ui';
import type { BaseDatePickerProps } from '@/components/ui/BaseDatePicker';
import { useApi } from '@/hooks/useApi';
import { DefaultApi, GetArtisanTimePostRequest, GetModelsPostRequest, Ticket } from '@/api';
import { AxiosRequestConfig } from 'axios';
import { ReactDatePickerProps } from 'react-datepicker';

export type DatePickerWithDynamicTimeProps = {
  request?: string;
  requiredFields: FormObject;
  requiredKey?: string;
  value: string | number | undefined;
  onChange: (date: string) => void;
};

const Api = new DefaultApi();

export const DatePickerWithDynamicTime: FC<
  DatePickerWithDynamicTimeProps &
    Pick<ReactDatePickerProps<never, false>, 'placeholderText' | 'disabled'> &
    Pick<BaseDatePickerProps, 'hasError'>
> = ({ request, requiredKey, value, onChange, requiredFields, ...rest }) => {
  const [selectedDate, setSelectedDate] = useState<Date | null>(value ? new Date(value) : null);
  const [availableTimes, setAvailableTimes] = useState<Date[]>([]);
  const [requestReady, setRequestReady] = useState<boolean>(false);
  const [clickedDay, setClickedDay] = useState<string>('');
  const { data, status } = useApi(
    `${request}_${requiredKey ? requiredFields[requiredKey] + new Date().getTime() : ''}`,
    () =>
      Api[request as keyof typeof Api]({ date: clickedDay, ...requiredFields } as string &
        Ticket &
        GetArtisanTimePostRequest &
        AxiosRequestConfig<never> &
        GetModelsPostRequest),
    requestReady,
  );

  useEffect(() => {
    if (status === 'error') {
      setAvailableTimes([]);
    } else if (status === 'fetched') {
      setSelectedDate(new Date(clickedDay));
      setAvailableTimes(Array.isArray(data) ? data.map((item) => new Date(typeof item === 'string' ? item : '')) : []);
    }
    setRequestReady(false);
  }, [data]);

  const onChangeHandler = async (date: Date, e: ChangeEvent<HTMLInputElement>) => {
    const day = new Date(date).toISOString();
    setClickedDay(day);
    if (!e?.target) {
      setSelectedDate(new Date(date));
      onChange(day);
      return;
    }
    setRequestReady(true);
  };
  const filterPassedTime = (time: Date) => new Date().getTime() < new Date(time).getTime();

  return (
    <BaseDatePicker
      dateFormat='dd.MM.yyyy HH:mm'
      showTimeSelect
      minDate={new Date()}
      includeTimes={availableTimes}
      filterTime={filterPassedTime}
      timeIntervals={60}
      timeCaption={availableTimes.length ? 'Доступное время' : 'Выберите другой день'}
      value={value}
      selected={selectedDate}
      onChange={onChangeHandler}
      {...rest}
    />
  );
};
