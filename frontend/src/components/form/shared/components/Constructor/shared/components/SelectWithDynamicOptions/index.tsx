import { FC, useEffect, useState } from 'react';
import { SelectValue } from '@/components/form/shared';
import type { BaseSelectProps } from '@/components/ui/BaseSelect';
import { BaseSelect } from '@/components/ui/BaseSelect';
import { MultiValue, SingleValue } from 'react-select';
import { useApi } from '@/hooks/useApi';
import { DefaultApi, Ticket } from '@/api';

export type SelectWithDynamicOptionsProps = {
  request?: string;
  requiredFields: Partial<Ticket>;
  requiredKey: string;
  disabled: boolean;
  value: string | number | undefined;
  onChange: (newValue: string) => void;
};

const Api = new DefaultApi();
export const SelectWithDynamicOptions: FC<
  SelectWithDynamicOptionsProps & Pick<BaseSelectProps, 'placeholder' | 'hasError'>
> = ({ value, request = '', requiredFields, requiredKey, disabled, onChange, ...rest }) => {
  const [options, setOptions] = useState<SelectValue[]>([]);
  const { status, data } = useApi(
    `${request}_${requiredKey ? requiredFields[requiredKey as keyof Ticket] : ''}`,
    () => Api[request as keyof typeof Api](requiredFields as never),
    requiredKey ? !!requiredFields[requiredKey as keyof Ticket] : true,
  );
  useEffect(() => {
    if (data) {
      setOptions(data as SelectValue[]);
    }
  }, [data]);
  const getOptionLabel = (option: SelectValue) => (typeof option !== 'string' ? option.name : option);

  const getOptionValue = (option: SelectValue) => (typeof option !== 'string' ? option.id : option);

  const getCurValue = () =>
    options.find((option: SelectValue) => (typeof option !== 'string' ? option.id === value : option === value));

  const onChangeHandler = (e: SingleValue<SelectValue> | MultiValue<SelectValue>) => {
    if (!e) return;
    if (typeof e === 'object') {
      if ('id' in e) {
        onChange(e.id);
      }
    } else {
      onChange(e);
    }
  };

  return (
    <BaseSelect
      isLoading={status === 'fetching'}
      isSearchable={false}
      options={options}
      isDisabled={disabled}
      value={getCurValue()}
      getOptionLabel={getOptionLabel}
      getOptionValue={getOptionValue}
      onChange={onChangeHandler}
      {...rest}
    />
  );
};
