import { BaseInput, BaseTextArea } from '@/components/ui';
import { DatePickerWithDynamicTime, SelectWithDynamicOptions } from './shared';
import { ChangeEvent, FC, useMemo } from 'react';
import type {
  FieldResponse,
  FieldInputItem,
  FieldSelectItem,
  FieldDateItem,
  DefaultObject,
} from '@/components/form/shared';
import styles from './index.module.scss';
import { useRequiredFields } from '@/hooks/useRequiredFields';
import { Ticket } from '@/api';

type FormFieldProps = {
  item: FieldInputItem | FieldSelectItem | FieldDateItem;
  value: string | number | undefined;
  errorsList: DefaultObject[];
  form: Ticket;
  onChange: (newValue: FieldResponse) => void;
};

export const FormField: FC<FormFieldProps> = ({ item, form, value, errorsList, onChange }) => {
  const reqFields = useRequiredFields(form, item.required as keyof Ticket);
  const isDisabled = useMemo(() => (item.required ? !form[item.required] : false), [form, item.required]);
  const hasError = useMemo(() => !!errorsList.find((e) => e[item.key]), [errorsList, item.key]);
  const onChangeHandler = (e: string | ChangeEvent<HTMLTextAreaElement>): void => {
    onChange({
      key: item.key,
      value: typeof e === 'string' ? e : e.target.value,
    });
  };

  const onInputChangeHandler = (e: ChangeEvent<HTMLInputElement>): void => {
    onChange({
      key: item.key,
      value: e.target.value,
    });
  };
  const baseProps = {
    key: item.key,
    value: value,
    hasError: hasError,
    disabled: isDisabled,
  };

  switch (item.type) {
    case 'select':
      return (
        <SelectWithDynamicOptions
          placeholder={item.label}
          request={item.itemsApi}
          requiredFields={reqFields}
          requiredKey={item.required}
          onChange={onChangeHandler}
          {...baseProps}
        />
      );
    case 'input':
      return (
        <BaseInput
          placeholder={item.label}
          mask={'mask' in item ? item.mask : null}
          onInput={onInputChangeHandler}
          {...baseProps}
        />
      );
    case 'date':
      return (
        <DatePickerWithDynamicTime
          placeholderText={item.label}
          request={item.itemsApi}
          requiredFields={reqFields as DefaultObject}
          requiredKey={item.required}
          onChange={onChangeHandler}
          {...baseProps}
        />
      );
    case 'text':
      return (
        <BaseTextArea
          className={styles['constructor__textarea']}
          placeholder={item.label}
          onChange={onChangeHandler}
          {...baseProps}
        />
      );
  }
  return <div>Элемент не найден</div>;
};
