import type { Step } from '@/components/form/shared/types';

export const FORM_CONFIG: Array<Step[]> = [
  [
    {
      key: crypto.randomUUID(),
      title: '',
      items: [
        {
          key: 'brand_id',
          label: 'Бренд',
          type: 'select',
          itemsApi: 'getBrandsGet',
          required: '',
        },
        {
          key: 'model_id',
          label: 'Модель',
          type: 'select',
          itemsApi: 'getModelsPost',
          required: 'brand_id',
        },
        {
          key: 'year',
          label: 'Год выпуска',
          type: 'input',
          mask: /^\d{0,4}$/,
          required: '',
        },
        {
          key: 'number',
          label: 'Гос. номер',
          type: 'input',
          required: '',
        },
        {
          key: 'vin',
          label: 'VIN номер',
          type: 'input',
          mask: /^.{0,17}$/,
          required: '',
        },
      ],
    },
  ],
  [
    {
      key: crypto.randomUUID(),
      title: '',
      items: [
        {
          key: 'service_id',
          label: 'Выберите услугу',
          type: 'select',
          itemsApi: 'getServicesGet',
          required: '',
        },
        {
          key: 'artisan_id',
          label: 'Мастер-приемщик',
          type: 'select',
          itemsApi: 'getArtisansGet',
          required: '',
        },
        {
          key: 'comment',
          label: 'Комментарий',
          type: 'text',
          required: '',
        },
      ],
    },
  ],
  [
    {
      key: crypto.randomUUID(),
      title: '',
      items: [
        {
          key: 'name',
          label: 'Ваше имя',
          type: 'input',
          required: '',
        },
        {
          key: 'phone',
          label: 'Номер телефона (с 7 или 8)',
          type: 'input',
          mask: /^((\+7|7|8)+([0-9]){0,10})$/,
          required: '',
        },
      ],
    },
    {
      key: crypto.randomUUID(),
      title: 'Выберите дату и время',
      items: [
        {
          key: 'time',
          label: 'Дата',
          type: 'date',
          itemsApi: 'getArtisanTimePost',
          required: 'artisan_id',
          showTime: true,
        },
      ],
    },
  ],
];

export const INITIAL_FORM = {
  artisan_id: undefined,
  brand_id: undefined,
  comment: '',
  model_id: undefined,
  name: '',
  number: '',
  phone: '',
  service_id: undefined,
  time: '',
  vin: '',
  year: null,
};
