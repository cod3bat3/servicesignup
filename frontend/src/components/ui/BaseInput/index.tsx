import styles from './index.module.scss';
import { FC, SelectHTMLAttributes } from 'react';
import { clsx } from 'clsx';
import { useMaskito } from '@maskito/react';
import { MaskitoMask } from '@maskito/core';

type InputProps = {
  mask?: MaskitoMask | null;
  hasError: boolean;
};

export const BaseInput: FC<SelectHTMLAttributes<HTMLInputElement> & InputProps> = ({
  value,
  mask,
  hasError,
  ...rest
}) => {
  const inputRef = mask
      // eslint-disable-next-line react-hooks/rules-of-hooks
    ? useMaskito({
        options: {
          mask: mask,
        },
      })
    : null;
  const props= mask ? { ...rest, ref: inputRef } : { ...rest };
  return (
    <div className={clsx(styles.input, { [styles.error]: hasError && !value })}>
      <input {...props} type='text' value={value || ''} />
    </div>
  );
};
