import DatePicker, { ReactDatePickerProps, registerLocale } from 'react-datepicker';
import ru from 'date-fns/locale/ru';
import 'react-datepicker/dist/react-datepicker.css';
import './index.scss';
import { FC } from 'react';
import { clsx } from 'clsx';

export type BaseDatePickerProps = {
  value: string | number | undefined;
  hasError: boolean;
};
registerLocale('ru', ru);

export const BaseDatePicker: FC<BaseDatePickerProps & Omit<ReactDatePickerProps<never, false>, 'value'>> = ({
  hasError,
  value,
  ...rest
}) => {
  return (
    <DatePicker className={clsx('datePicker', { ['error']: hasError && !value })} locale='ru' {...rest} />
  );
};
