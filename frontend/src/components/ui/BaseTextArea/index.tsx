import { FC, SelectHTMLAttributes } from 'react';
import styles from './index.module.scss';
import { clsx } from 'clsx';

type BaseTextAreaProps = {
  hasError: boolean;
};
export const BaseTextArea: FC<SelectHTMLAttributes<HTMLTextAreaElement> & BaseTextAreaProps> = ({
  hasError,
  className,
  value,
  ...rest
}) => {
  return (
    <textarea
      className={clsx(styles.textarea, className, { [styles.error]: hasError && !value })}
      value={value}
      {...rest}
    />
  );
};
