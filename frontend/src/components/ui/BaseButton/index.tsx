import { FC, PropsWithChildren, SelectHTMLAttributes } from 'react';
import styles from './index.module.scss';
import { clsx } from 'clsx';

export type ButtonProps = {
  mode?: 'outlined' | 'default';
};

export const BaseButton: FC<PropsWithChildren<SelectHTMLAttributes<HTMLButtonElement> & ButtonProps>> = ({
  children,
  mode = 'default',
  className,
  ...rest
}) => {
  return (
    <button type='button' className={clsx(styles.button, className, styles[`button--${mode}`])} {...rest}>
      {children}
    </button>
  );
};
