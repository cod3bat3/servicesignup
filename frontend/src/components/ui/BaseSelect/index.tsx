import Select, { MultiValue, PropsValue, SingleValue } from 'react-select';
import { styles } from './shared';
import customStyles from './index.module.scss';
import { FC } from 'react';
import { SelectValue } from '@/components/form/shared';
import { clsx } from 'clsx';

export type BaseSelectProps = {
  value: PropsValue<SelectValue> | undefined;
  hasError: boolean;
  isDisabled: boolean;
  isLoading?: boolean;
  isSearchable?: boolean;
  placeholder: string;
  options: SelectValue[];
  getOptionLabel: (option: SelectValue) => string;
  getOptionValue: (option: SelectValue) => string;
  onChange: (e: SingleValue<SelectValue> | MultiValue<SelectValue>) => void;
};

export const BaseSelect: FC<BaseSelectProps> = ({ hasError, value, ...rest }) => {
  return (
    <Select className={clsx({ [customStyles.error]: hasError && !value })} styles={styles} value={value} {...rest} />
  );
};
