import { GroupBase, StylesConfig } from 'react-select';
import { SelectValue } from '@/components/form/shared';

export const styles: StylesConfig<SelectValue, boolean, GroupBase<SelectValue>> | undefined = {
  indicatorSeparator: () => ({
    display: 'none',
  }),
  control: (baseStyles) => ({
    ...baseStyles,
    borderRadius: 'var(--radius-df)',
    border: '1px solid var(--color-border)',
    minHeight: '45px',
    '&:hover': {
      borderColor: 'var(--color-border)',
    },
  }),
  placeholder: (baseStyles) => ({
    ...baseStyles,
    color: 'var(--color-text-primary)',
    fontSize: '14px',
  }),
  singleValue: (baseStyles) => ({
    ...baseStyles,
    fontSize: '14px',
  }),
  dropdownIndicator: (baseStyles) => ({
    ...baseStyles,
    color: 'var(--color-primary)',
    '&:hover': {
      color: 'var(--color-primary)',
    },
    svg: {
      width: '24px',
      height: '24px',
    },
  }),
  option: (baseStyles, { isSelected }) => ({
    ...baseStyles,
    fontSize: '14px',
    backgroundColor: isSelected ? 'var(--color-primary)' : undefined,
  }),
};
